# CompleteBranchCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Sha** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**Commit** | [***CompleteBranchCommitCommit**](CompleteBranch_commit_commit.md) |  | [optional] [default to null]
**Author** | [***UserMini**](UserMini.md) |  | [optional] [default to null]
**Parents** | [**[]BranchCommit**](Branch_commit.md) |  | [optional] [default to null]
**Committer** | [***UserMini**](UserMini.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

