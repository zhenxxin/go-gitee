# OwnerRepoBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Name** | **string** | 仓库名称 | [default to null]
**Description** | **string** | 仓库描述 | [optional] [default to null]
**Homepage** | **string** | 主页(eg: https://gitee.com) | [optional] [default to null]
**HasIssues** | **bool** | 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
**HasWiki** | **bool** | 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
**CanComment** | **bool** | 允许用户对仓库进行评论。默认： 允许(true) | [optional] [default to true]
**IssueComment** | **bool** | 允许对“关闭”状态的 Issue 进行评论。默认: 不允许(false) | [optional] [default to null]
**SecurityHoleEnabled** | **bool** | 这个Issue涉及到安全/隐私问题，提交后不公开此Issue（可见范围：仓库成员, 企业成员） | [optional] [default to null]
**Private** | **bool** | 仓库公开或私有。 | [optional] [default to null]
**Path** | **string** | 更新仓库路径 | [optional] [default to null]
**DefaultBranch** | **string** | 更新默认分支 | [optional] [default to null]
**PullRequestsEnabled** | **bool** | 接受 pull request，协作开发 | [optional] [default to null]
**OnlineEditEnabled** | **bool** | 是否允许仓库文件在线编辑 | [optional] [default to null]
**LightweightPrEnabled** | **bool** | 是否接受轻量级 pull request | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

