# MembersUsernameBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Role** | **string** | 企业角色：member &#x3D;&gt; 普通成员, outsourced &#x3D;&gt; 外包成员, admin &#x3D;&gt; 管理员 | [optional] [default to ROLE.MEMBER]
**Active** | **bool** | 是否可访问企业资源，默认:是。（若选否则禁止该用户访问企业资源） | [optional] [default to true]
**Name** | **string** | 企业成员真实姓名（备注） | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

