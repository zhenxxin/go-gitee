# GiteeMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | **string** |  | [optional] [default to null]
**TotalScore** | **string** |  | [optional] [default to null]
**CreatedAt** | **string** |  | [optional] [default to null]
**Repo** | [***ProjectBasic**](ProjectBasic.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

