# ProjectBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**FullName** | **string** |  | [optional] [default to null]
**HumanName** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**Namespace** | [***NamespaceMini**](NamespaceMini.md) |  | [optional] [default to null]
**Path** | **string** | 仓库路径 | [optional] [default to null]
**Name** | **string** | 仓库名称 | [optional] [default to null]
**Assigner** | [***UserBasic**](UserBasic.md) |  | [optional] [default to null]
**Description** | **string** | 仓库描述 | [optional] [default to null]
**Private** | **bool** | 是否私有 | [optional] [default to null]
**Public** | **bool** | 是否公开 | [optional] [default to null]
**Internal** | **string** | 是否内部开源 | [optional] [default to null]
**Fork** | **bool** | 是否是fork仓库 | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**SshUrl** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

