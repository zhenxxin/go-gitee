# EnterpriseReposBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Name** | **string** | 仓库名称 | [default to null]
**Description** | **string** | 仓库描述 | [optional] [default to null]
**Homepage** | **string** | 主页(eg: https://gitee.com) | [optional] [default to null]
**HasIssues** | **bool** | 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
**HasWiki** | **bool** | 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
**CanComment** | **bool** | 允许用户对仓库进行评论。默认： 允许(true) | [optional] [default to true]
**AutoInit** | **bool** | 值为true时则会用README初始化仓库。默认: 不初始化(false) | [optional] [default to null]
**GitignoreTemplate** | **string** | Git Ignore模版 | [optional] [default to null]
**LicenseTemplate** | **string** | License模版 | [optional] [default to null]
**Path** | **string** | 仓库路径 | [optional] [default to null]
**Private** | **int32** | 仓库开源类型。0(私有), 1(外部开源), 2(内部开源)。默认: 0 | [optional] [default to PRIVATE.0_]
**Outsourced** | **bool** | 值为true值为外包仓库, false值为内部仓库。默认: 内部仓库(false) | [optional] [default to null]
**ProjectCreator** | **string** | 负责人的username | [optional] [default to null]
**Members** | **string** | 用逗号分开的仓库成员。如: member1,member2 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

