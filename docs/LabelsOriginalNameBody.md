# LabelsOriginalNameBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Name** | **string** | 标签新名称 | [optional] [default to null]
**Color** | **string** | 标签新颜色 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

