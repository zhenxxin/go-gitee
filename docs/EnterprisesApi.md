# {{classname}}

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5EnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#DeleteV5EnterprisesEnterpriseMembersUsername) | **Delete** /v5/enterprises/{enterprise}/members/{username} | 移除企业成员
[**DeleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId**](EnterprisesApi.md#DeleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId) | **Delete** /v5/enterprises/{enterprise}/week_reports/{report_id}/comments/{id} | 删除周报某个评论
[**GetV5EnterpriseEnterprisePullRequests**](EnterprisesApi.md#GetV5EnterpriseEnterprisePullRequests) | **Get** /v5/enterprise/{enterprise}/pull_requests | 企业Pull Reuqest 列表
[**GetV5EnterprisesEnterprise**](EnterprisesApi.md#GetV5EnterprisesEnterprise) | **Get** /v5/enterprises/{enterprise} | 获取一个企业
[**GetV5EnterprisesEnterpriseMembers**](EnterprisesApi.md#GetV5EnterprisesEnterpriseMembers) | **Get** /v5/enterprises/{enterprise}/members | 列出企业的所有成员
[**GetV5EnterprisesEnterpriseMembersSearch**](EnterprisesApi.md#GetV5EnterprisesEnterpriseMembersSearch) | **Get** /v5/enterprises/{enterprise}/members/search | 获取企业成员信息(通过用户名/邮箱)
[**GetV5EnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#GetV5EnterprisesEnterpriseMembersUsername) | **Get** /v5/enterprises/{enterprise}/members/{username} | 获取企业的一个成员
[**GetV5EnterprisesEnterpriseUsersUsernameWeekReports**](EnterprisesApi.md#GetV5EnterprisesEnterpriseUsersUsernameWeekReports) | **Get** /v5/enterprises/{enterprise}/users/{username}/week_reports | 个人周报列表
[**GetV5EnterprisesEnterpriseWeekReports**](EnterprisesApi.md#GetV5EnterprisesEnterpriseWeekReports) | **Get** /v5/enterprises/{enterprise}/week_reports | 企业成员周报列表
[**GetV5EnterprisesEnterpriseWeekReportsId**](EnterprisesApi.md#GetV5EnterprisesEnterpriseWeekReportsId) | **Get** /v5/enterprises/{enterprise}/week_reports/{id} | 周报详情
[**GetV5EnterprisesEnterpriseWeekReportsIdComments**](EnterprisesApi.md#GetV5EnterprisesEnterpriseWeekReportsIdComments) | **Get** /v5/enterprises/{enterprise}/week_reports/{id}/comments | 某个周报评论列表
[**GetV5UserEnterprises**](EnterprisesApi.md#GetV5UserEnterprises) | **Get** /v5/user/enterprises | 列出授权用户所属的企业
[**PatchV5EnterprisesEnterpriseWeekReportId**](EnterprisesApi.md#PatchV5EnterprisesEnterpriseWeekReportId) | **Patch** /v5/enterprises/{enterprise}/week_report/{id} | 编辑周报
[**PostV5EnterprisesEnterpriseMembers**](EnterprisesApi.md#PostV5EnterprisesEnterpriseMembers) | **Post** /v5/enterprises/{enterprise}/members | 添加或邀请企业成员
[**PostV5EnterprisesEnterpriseWeekReport**](EnterprisesApi.md#PostV5EnterprisesEnterpriseWeekReport) | **Post** /v5/enterprises/{enterprise}/week_report | 新建周报
[**PostV5EnterprisesEnterpriseWeekReportsIdComment**](EnterprisesApi.md#PostV5EnterprisesEnterpriseWeekReportsIdComment) | **Post** /v5/enterprises/{enterprise}/week_reports/{id}/comment | 评论周报
[**PutV5EnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#PutV5EnterprisesEnterpriseMembersUsername) | **Put** /v5/enterprises/{enterprise}/members/{username} | 修改企业成员权限或备注

# **DeleteV5EnterprisesEnterpriseMembersUsername**
> DeleteV5EnterprisesEnterpriseMembersUsername(ctx, enterprise, username, optional)
移除企业成员

移除企业成员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
  **username** | **string**| 用户名(username/login) | 
 **optional** | ***EnterprisesApiDeleteV5EnterprisesEnterpriseMembersUsernameOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiDeleteV5EnterprisesEnterpriseMembersUsernameOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId**
> DeleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId(ctx, enterprise, reportId, id, optional)
删除周报某个评论

删除周报某个评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
  **reportId** | **int32**| 周报ID | 
  **id** | **int32**| 评论ID | 
 **optional** | ***EnterprisesApiDeleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsIdOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiDeleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsIdOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterpriseEnterprisePullRequests**
> []PullRequest GetV5EnterpriseEnterprisePullRequests(ctx, enterprise, optional)
企业Pull Reuqest 列表

企业Pull Reuqest 列表

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
 **optional** | ***EnterprisesApiGetV5EnterpriseEnterprisePullRequestsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterpriseEnterprisePullRequestsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **accessToken** | **optional.String**| 用户授权码 | 
 **issueNumber** | **optional.String**| 可选。Issue 编号(区分大小写，无需添加 # 号) | 
 **repo** | **optional.String**| 可选。仓库路径(path) | 
 **programId** | **optional.Int32**| 可选。项目ID | 
 **state** | **optional.String**| 可选。Pull Request 状态 | [default to open]
 **head** | **optional.String**| 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch | 
 **base** | **optional.String**| 可选。Pull Request 提交目标分支的名称。 | 
 **sort** | **optional.String**| 可选。排序字段，默认按创建时间 | [default to created]
 **since** | **optional.String**| 可选。起始的更新时间，要求时间格式为 ISO 8601 | 
 **direction** | **optional.String**| 可选。升序/降序 | [default to desc]
 **milestoneNumber** | **optional.Int32**| 可选。里程碑序号(id) | 
 **labels** | **optional.String**| 用逗号分开的标签。如: bug,performance | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]

### Return type

[**[]PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterprisesEnterprise**
> EnterpriseBasic GetV5EnterprisesEnterprise(ctx, enterprise, optional)
获取一个企业

获取一个企业

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
 **optional** | ***EnterprisesApiGetV5EnterprisesEnterpriseOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterprisesEnterpriseOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**EnterpriseBasic**](EnterpriseBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterprisesEnterpriseMembers**
> []EnterpriseMember GetV5EnterprisesEnterpriseMembers(ctx, enterprise, optional)
列出企业的所有成员

列出企业的所有成员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
 **optional** | ***EnterprisesApiGetV5EnterprisesEnterpriseMembersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterprisesEnterpriseMembersOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **accessToken** | **optional.String**| 用户授权码 | 
 **role** | **optional.String**| 根据角色筛选成员 | [default to all]
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]

### Return type

[**[]EnterpriseMember**](EnterpriseMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterprisesEnterpriseMembersSearch**
> GetV5EnterprisesEnterpriseMembersSearch(ctx, enterprise, queryType, queryValue, optional)
获取企业成员信息(通过用户名/邮箱)

获取企业成员信息(通过用户名/邮箱)

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
  **queryType** | **string**| 查询类型：username/email | 
  **queryValue** | **string**| 查询值 | 
 **optional** | ***EnterprisesApiGetV5EnterprisesEnterpriseMembersSearchOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterprisesEnterpriseMembersSearchOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterprisesEnterpriseMembersUsername**
> EnterpriseMember GetV5EnterprisesEnterpriseMembersUsername(ctx, enterprise, username, optional)
获取企业的一个成员

获取企业的一个成员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
  **username** | **string**| 用户名(username/login) | 
 **optional** | ***EnterprisesApiGetV5EnterprisesEnterpriseMembersUsernameOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterprisesEnterpriseMembersUsernameOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**EnterpriseMember**](EnterpriseMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterprisesEnterpriseUsersUsernameWeekReports**
> []WeekReport GetV5EnterprisesEnterpriseUsersUsernameWeekReports(ctx, enterprise, username, optional)
个人周报列表

个人周报列表

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
  **username** | **string**| 用户名(username/login) | 
 **optional** | ***EnterprisesApiGetV5EnterprisesEnterpriseUsersUsernameWeekReportsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterprisesEnterpriseUsersUsernameWeekReportsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accessToken** | **optional.String**| 用户授权码 | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]

### Return type

[**[]WeekReport**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterprisesEnterpriseWeekReports**
> []WeekReport GetV5EnterprisesEnterpriseWeekReports(ctx, enterprise, optional)
企业成员周报列表

企业成员周报列表

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
 **optional** | ***EnterprisesApiGetV5EnterprisesEnterpriseWeekReportsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterprisesEnterpriseWeekReportsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **accessToken** | **optional.String**| 用户授权码 | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]
 **username** | **optional.String**| 用户名(username/login) | 
 **year** | **optional.Int32**| 周报所属年 | 
 **weekIndex** | **optional.Int32**| 周报所属周 | 
 **date** | **optional.String**| 周报日期(格式：2019-03-25) | 

### Return type

[**[]WeekReport**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterprisesEnterpriseWeekReportsId**
> WeekReport GetV5EnterprisesEnterpriseWeekReportsId(ctx, enterprise, id, optional)
周报详情

周报详情

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
  **id** | **int32**| 周报ID | 
 **optional** | ***EnterprisesApiGetV5EnterprisesEnterpriseWeekReportsIdOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterprisesEnterpriseWeekReportsIdOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5EnterprisesEnterpriseWeekReportsIdComments**
> []Note GetV5EnterprisesEnterpriseWeekReportsIdComments(ctx, enterprise, id, optional)
某个周报评论列表

某个周报评论列表

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
  **id** | **int32**| 周报ID | 
 **optional** | ***EnterprisesApiGetV5EnterprisesEnterpriseWeekReportsIdCommentsOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5EnterprisesEnterpriseWeekReportsIdCommentsOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accessToken** | **optional.String**| 用户授权码 | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]

### Return type

[**[]Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserEnterprises**
> []EnterpriseBasic GetV5UserEnterprises(ctx, optional)
列出授权用户所属的企业

列出授权用户所属的企业

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***EnterprisesApiGetV5UserEnterprisesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiGetV5UserEnterprisesOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **optional.String**| 用户授权码 | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]
 **admin** | **optional.Bool**| 只列出授权用户管理的企业 | [default to true]

### Return type

[**[]EnterpriseBasic**](EnterpriseBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5EnterprisesEnterpriseWeekReportId**
> WeekReport PatchV5EnterprisesEnterpriseWeekReportId(ctx, accessToken, content, enterprise, id)
编辑周报

编辑周报

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **accessToken** | **string**|  | 
  **content** | **string**|  | 
  **enterprise** | **string**| 企业的路径(path/login) | 
  **id** | **int32**| 周报ID | 

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5EnterprisesEnterpriseMembers**
> PostV5EnterprisesEnterpriseMembers(ctx, enterprise, optional)
添加或邀请企业成员

添加或邀请企业成员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
 **optional** | ***EnterprisesApiPostV5EnterprisesEnterpriseMembersOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiPostV5EnterprisesEnterpriseMembersOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **body** | [**optional.Interface of EnterpriseMembersBody**](EnterpriseMembersBody.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5EnterprisesEnterpriseWeekReport**
> WeekReport PostV5EnterprisesEnterpriseWeekReport(ctx, body, enterprise)
新建周报

新建周报

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**EnterpriseWeekReportBody**](EnterpriseWeekReportBody.md)|  | 
  **enterprise** | **string**| 企业的路径(path/login) | 

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5EnterprisesEnterpriseWeekReportsIdComment**
> Note PostV5EnterprisesEnterpriseWeekReportsIdComment(ctx, body, enterprise, id)
评论周报

评论周报

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**IdCommentBody**](IdCommentBody.md)|  | 
  **enterprise** | **string**| 企业的路径(path/login) | 
  **id** | **int32**| 周报ID | 

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5EnterprisesEnterpriseMembersUsername**
> EnterpriseMember PutV5EnterprisesEnterpriseMembersUsername(ctx, enterprise, username, optional)
修改企业成员权限或备注

修改企业成员权限或备注

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **enterprise** | **string**| 企业的路径(path/login) | 
  **username** | **string**| 用户名(username/login) | 
 **optional** | ***EnterprisesApiPutV5EnterprisesEnterpriseMembersUsernameOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a EnterprisesApiPutV5EnterprisesEnterpriseMembersUsernameOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **body** | [**optional.Interface of MembersUsernameBody**](MembersUsernameBody.md)|  | 

### Return type

[**EnterpriseMember**](EnterpriseMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

