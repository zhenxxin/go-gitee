# CompleteBranchCommitCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Author** | [***UserShort**](UserShort.md) |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**Message** | **string** |  | [optional] [default to null]
**Tree** | [***BranchCommit**](Branch_commit.md) |  | [optional] [default to null]
**Committer** | [***UserShort**](UserShort.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

