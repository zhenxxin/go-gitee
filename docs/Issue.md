# Issue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**RepositoryUrl** | **string** |  | [optional] [default to null]
**LabelsUrl** | **string** |  | [optional] [default to null]
**CommentsUrl** | **string** |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**ParentUrl** | **string** |  | [optional] [default to null]
**Number** | **string** | 唯一标识 | [optional] [default to null]
**ParentId** | **int32** | 上级 id | [optional] [default to null]
**Depth** | **int32** | 所在层级 | [optional] [default to null]
**State** | **string** | 状态 | [optional] [default to null]
**Title** | **string** | 标题 | [optional] [default to null]
**Body** | **string** | 描述 | [optional] [default to null]
**BodyHtml** | **string** | 描述 html 格式 | [optional] [default to null]
**Labels** | [***Label**](Label.md) |  | [optional] [default to null]
**Assignee** | [***UserBasic**](UserBasic.md) |  | [optional] [default to null]
**Collaborators** | [***UserBasic**](UserBasic.md) |  | [optional] [default to null]
**Repository** | [***Project**](Project.md) |  | [optional] [default to null]
**Milestone** | [***Milestone**](Milestone.md) |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) | 创建时间 | [optional] [default to null]
**UpdatedAt** | [**time.Time**](time.Time.md) | 更新时间 | [optional] [default to null]
**PlanStartedAt** | [**time.Time**](time.Time.md) | 计划开始时间 | [optional] [default to null]
**Deadline** | [**time.Time**](time.Time.md) | 结束时间 | [optional] [default to null]
**FinishedAt** | [**time.Time**](time.Time.md) | 完成时间 | [optional] [default to null]
**ScheduledTime** | **int32** | 预计工期 | [optional] [default to null]
**Comments** | **int32** | 评论数量 | [optional] [default to null]
**Priority** | **int32** | 优先级(0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重) | [optional] [default to null]
**IssueType** | **string** | 任务类型 | [optional] [default to null]
**Program** | [***ProgramBasic**](ProgramBasic.md) |  | [optional] [default to null]
**SecurityHole** | **bool** | 是否为私有issue | [optional] [default to null]
**IssueState** | **string** |  | [optional] [default to null]
**Branch** | **string** | 关联分支 | [optional] [default to null]
**IssueTypeDetail** | [***IssueType**](IssueType.md) |  | [optional] [default to null]
**IssueStateDetail** | [***IssueState**](IssueState.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

