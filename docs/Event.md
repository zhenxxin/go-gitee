# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Actor** | [***UserBasic**](UserBasic.md) |  | [optional] [default to null]
**Repo** | [***ProjectBasic**](ProjectBasic.md) |  | [optional] [default to null]
**Org** | **string** |  | [optional] [default to null]
**Public** | **bool** |  | [optional] [default to null]
**CreatedAt** | **string** |  | [optional] [default to null]
**Payload** | [***interface{}**](interface{}.md) | 不同类型动态的内容 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

