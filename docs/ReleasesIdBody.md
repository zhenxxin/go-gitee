# ReleasesIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**TagName** | **string** | Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 | [default to null]
**Name** | **string** | Release 名称 | [default to null]
**Body** | **string** | Release 描述 | [default to null]
**Prerelease** | **bool** | 是否为预览版本。默认: false（非预览版本） | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

