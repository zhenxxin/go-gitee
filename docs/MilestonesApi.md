# {{classname}}

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#DeleteV5ReposOwnerRepoMilestonesNumber) | **Delete** /v5/repos/{owner}/{repo}/milestones/{number} | 删除仓库单个里程碑
[**GetV5ReposOwnerRepoMilestones**](MilestonesApi.md#GetV5ReposOwnerRepoMilestones) | **Get** /v5/repos/{owner}/{repo}/milestones | 获取仓库所有里程碑
[**GetV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#GetV5ReposOwnerRepoMilestonesNumber) | **Get** /v5/repos/{owner}/{repo}/milestones/{number} | 获取仓库单个里程碑
[**PatchV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#PatchV5ReposOwnerRepoMilestonesNumber) | **Patch** /v5/repos/{owner}/{repo}/milestones/{number} | 更新仓库里程碑
[**PostV5ReposOwnerRepoMilestones**](MilestonesApi.md#PostV5ReposOwnerRepoMilestones) | **Post** /v5/repos/{owner}/{repo}/milestones | 创建仓库里程碑

# **DeleteV5ReposOwnerRepoMilestonesNumber**
> DeleteV5ReposOwnerRepoMilestonesNumber(ctx, owner, repo, number, optional)
删除仓库单个里程碑

删除仓库单个里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 里程碑序号(id) | 
 **optional** | ***MilestonesApiDeleteV5ReposOwnerRepoMilestonesNumberOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a MilestonesApiDeleteV5ReposOwnerRepoMilestonesNumberOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoMilestones**
> []Milestone GetV5ReposOwnerRepoMilestones(ctx, owner, repo, optional)
获取仓库所有里程碑

获取仓库所有里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
 **optional** | ***MilestonesApiGetV5ReposOwnerRepoMilestonesOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a MilestonesApiGetV5ReposOwnerRepoMilestonesOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accessToken** | **optional.String**| 用户授权码 | 
 **state** | **optional.String**| 里程碑状态: open, closed, all。默认: open | [default to open]
 **sort** | **optional.String**| 排序方式: due_on | [default to due_on]
 **direction** | **optional.String**| 升序(asc)或是降序(desc)。默认: asc | 
 **page** | **optional.Int32**| 当前的页码 | [default to 1]
 **perPage** | **optional.Int32**| 每页的数量，最大为 100 | [default to 20]

### Return type

[**[]Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoMilestonesNumber**
> Milestone GetV5ReposOwnerRepoMilestonesNumber(ctx, owner, repo, number, optional)
获取仓库单个里程碑

获取仓库单个里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 里程碑序号(id) | 
 **optional** | ***MilestonesApiGetV5ReposOwnerRepoMilestonesNumberOpts** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a pointer to a MilestonesApiGetV5ReposOwnerRepoMilestonesNumberOpts struct
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **accessToken** | **optional.String**| 用户授权码 | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoMilestonesNumber**
> Milestone PatchV5ReposOwnerRepoMilestonesNumber(ctx, accessToken, title, state, description, dueOn, owner, repo, number)
更新仓库里程碑

更新仓库里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **accessToken** | **string**|  | 
  **title** | **string**|  | 
  **state** | **string**|  | 
  **description** | **string**|  | 
  **dueOn** | **string**|  | 
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 
  **number** | **int32**| 里程碑序号(id) | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoMilestones**
> Milestone PostV5ReposOwnerRepoMilestones(ctx, body, owner, repo)
创建仓库里程碑

创建仓库里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**RepoMilestonesBody**](RepoMilestonesBody.md)|  | 
  **owner** | **string**| 仓库所属空间地址(企业、组织或个人的地址path) | 
  **repo** | **string**| 仓库路径(path) | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

