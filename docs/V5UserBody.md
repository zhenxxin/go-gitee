# V5UserBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | 用户授权码 | [optional] [default to null]
**Name** | **string** | 昵称 | [optional] [default to null]
**Blog** | **string** | 微博链接 | [optional] [default to null]
**Weibo** | **string** | 博客站点 | [optional] [default to null]
**Bio** | **string** | 自我介绍 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

