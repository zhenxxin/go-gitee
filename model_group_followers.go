/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

// 列出指定组织的所有关注者
type GroupFollowers struct {
	Self *UserBasic `json:"self,omitempty" xml:"self"`
	FollowedAt string `json:"followed_at,omitempty" xml:"followed_at"`
}
