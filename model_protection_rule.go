/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

// 删除仓库保护分支策略
type ProtectionRule struct {
	Id string `json:"id,omitempty" xml:"id"`
	ProjectId string `json:"project_id,omitempty" xml:"project_id"`
	Wildcard string `json:"wildcard,omitempty" xml:"wildcard"`
	Pushers []string `json:"pushers,omitempty" xml:"pushers"`
	Mergers []string `json:"mergers,omitempty" xml:"mergers"`
}
