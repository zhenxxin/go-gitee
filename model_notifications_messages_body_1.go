/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

type NotificationsMessagesBody1 struct {
	// 用户授权码
	AccessToken string `json:"access_token,omitempty" xml:"access_token"`
	// 用户名(username/login)
	Username string `json:"username" xml:"username"`
	// 私信内容
	Content string `json:"content" xml:"content"`
}
