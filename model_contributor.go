/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

// 获取仓库贡献者
type Contributor struct {
	Email string `json:"email,omitempty" xml:"email"`
	Name string `json:"name,omitempty" xml:"name"`
	Contributions string `json:"contributions,omitempty" xml:"contributions"`
}
