/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

// 获取一个企业
type EnterpriseBasic struct {
	// 企业ID
	Id int32 `json:"id,omitempty" xml:"id"`
	// 企业命名空间
	Path string `json:"path,omitempty" xml:"path"`
	// 企业名称
	Name string `json:"name,omitempty" xml:"name"`
	// 企业地址
	Url string `json:"url,omitempty" xml:"url"`
	// 企业头像地址
	AvatarUrl string `json:"avatar_url,omitempty" xml:"avatar_url"`
}
