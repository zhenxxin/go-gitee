/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 5.4.31
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package gitee

type RepoBaiduStatisticKeyBody struct {
	// 用户授权码
	AccessToken string `json:"access_token,omitempty" xml:"access_token"`
	// 通过百度统计页面获取的 hm.js? 后面的 key
	Key string `json:"key,omitempty" xml:"key"`
}
